CREATE TABLE woerter
(
   Id INTEGER PRIMARY KEY NOT NULL,
   Wort VARCHAR 
);

CREATE TABLE Truth
(
   Id INTEGER PRIMARY KEY NOT NULL,
   Truth_wort VARCHAR
);
        
CREATE TABLE Lie
(
   Id INTEGER PRIMARY KEY NOT NULL,
   Lie_wort VARCHAR
);

CREATE TABLE Questions
( 
    Id INTEGER PRIMARY KEY NOT NULL,
    Wort_Id INTEGER REFERENCES woerter(Id),
    Truth_Id INTEGER REFERENCES Truth(Id),
    Lie_Id INTEGER REFERENCES Lie(Id)
);
