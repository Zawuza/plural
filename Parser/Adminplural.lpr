program Adminplural;
uses SysUtils, sqldb, pqconnection;
var PrimKey:integer;
    Word,TrueWord,FalseWord:string;
    KeinPluralArray: array [1..2] of boolean;
    {}
    Connection  :  TPQConnection;
    Transaction :  TSQLTransaction;
    Query       :  TSQLScript;

function MakeStringForQuery(a,b:boolean; prim:integer):string;
var str:string;
begin
  if a=true then str:='0,'
  else           str:=IntToStr(prim) + ',';
  if b=true then str:= str + '0'
  else           str:= str + IntToStr(prim);
  Result:=str;
end;

procedure StatementGeneration;
{SQL statement generation}
{Generation of truth and lie}
begin
  if TrueWord<>'kein_plural' then
       begin
       Query.Script.Text:=Query.Script.Text + 'INSERT INTO Truth VALUES (' + IntToStr(PrimKey) + ',''' +
                            TrueWord + ''');';
       KeinPluralArray[1]:=false
       end
    else
       begin
       KeinPluralArray[1]:=true
       end;
   {--------}
   if FalseWord<>'kein_plural' then
       begin
       Query.Script.Text:=Query.Script.Text + 'INSERT INTO lie VALUES (' + IntToStr(PrimKey) + ',''' +
                            FalseWord + ''');';
       KeinPluralArray[2]:=false;
       end
    else
       begin
       KeinPluralArray[2]:=true;
       end;
    Query.Script.Text:=Query.Script.Text + 'Insert INTO Questions VALUES (' + IntToStr(PrimKey) +',' +
                                   IntToStr(PrimKey) + ',' +
                                   MakeStringForQuery(KeinPluralArray[1],KeinPluralArray[2],PrimKey) + ');';
end;

begin
  if ParamCount<4 then
     begin
       Writeln('Not enough arguments!');
       Writeln('You must set 4 args:');
       Writeln('1) Number for primary key');
       Writeln('2) Three words: word, right plural, false plural (kein_plural if the word has no plural');
       halt;
     end;
  try
  PrimKey:=StrToInt(ParamStr(1));
  except
    Writeln('Not right params:');
    Writeln('You must set 4 args:');
    Writeln('1) Number for primary key');
    Writeln('2) Three words: word, right plural, false plural (kein_plural if the word has no plural');
    halt;
  end;
  Word:=ParamStr(2);
  TrueWord:=ParamStr(3);
  FalseWord:=ParamStr(4);

  Connection:=TPQConnection.Create(nil);
  Transaction:=TSQLTransaction.Create(Connection);
  Query:=TSQLScript.Create(Connection);

  Connection.HostName:='pellefant-01.db.elephantsql.com';
  Connection.DatabaseName:='****';
  Connection.Username:='****';
  Connection.Password:='***';

  Transaction.Database:=Connection;

  Query.Database:=Connection;
  Query.Transaction:=Transaction;
  Query.Script.Text:='INSERT INTO woerter VALUES (' + IntToStr(PrimKey) + ',''' +
                          Word + '''); ';
  {Generate SQL}
  StatementGeneration;
  {Connect to databaseand execute SQL}
  try
     write(Query.Script.Text);
     Connection.Connected:=true;
     Query.Execute;
     Transaction.Commit;
     Writeln('In base');
  finally
     Connection.Free;
  end;
end.

