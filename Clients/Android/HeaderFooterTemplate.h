//---------------------------------------------------------------------------

#ifndef HeaderFooterTemplateH
#define HeaderFooterTemplateH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <Data.DB.hpp>
#include "MemDS.hpp"
#include "PgAccess.hpp"
#include "DBAccess.hpp"
//---------------------------------------------------------------------------
class TfMain : public TForm
{
__published:	// IDE-managed Components
	TToolBar *Header;
	TLabel *HeaderLabel;
	TLabel *MainMenuLabel;
	TButton *NewGameButton;
	TButton *AboutButton;
	TAniIndicator *aind;
	TLabel *WortLabel;
	TButton *Antwort1;
	TButton *Antwort2;
	TLabel *LoadingLabel;
	TTimer *Timer1;
	TPgConnection *PgConnection1;
	TPgQuery *PgQuery1;
	void __fastcall NewGameButtonClick(TObject *Sender);
	void __fastcall LoadFromBase();
	void __fastcall Antwort1Click(TObject *Sender);
	void __fastcall Antwort2Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:	UnicodeString wort, true_wort, false_wort;
		bool umkehr,stopgame;
	__fastcall TfMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfMain *fMain;
//---------------------------------------------------------------------------
#endif
