//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "HeaderFooterTemplate.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBAccess"
#pragma link "MemDS"
#pragma link "PgAccess"
#pragma link "DBAccess"
#pragma resource "*.fmx"
#pragma resource ("*.SmXhdpiPh.fmx", _PLAT_ANDROID)
#pragma resource ("*.NmXhdpiPh.fmx", _PLAT_ANDROID)
#pragma resource ("*.LgXhdpiPh.fmx", _PLAT_ANDROID)

TfMain *fMain;
//---------------------------------------------------------------------------
__fastcall TfMain::TfMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfMain::NewGameButtonClick(TObject *Sender)
{
fMain->NewGameButton->Visible=false;
fMain->AboutButton->Visible=false;
fMain->MainMenuLabel->Visible=false;
fMain->aind->Visible=true;
fMain->LoadingLabel->Visible=true;
fMain->LoadFromBase();
fMain->aind->Visible=false;
fMain->LoadingLabel->Visible=false;
fMain->WortLabel->Text=wort;
if (umkehr)
{
   fMain->Antwort1->Text=false_wort;
   fMain->Antwort2->Text=true_wort;
}
else
{
   fMain->Antwort1->Text=true_wort;
   fMain->Antwort2->Text=false_wort;
}
fMain->WortLabel->Visible=true;
fMain->Antwort1->Visible=true;
fMain->Antwort2->Visible=true;
stopgame=false;
}
//---------------------------------------------------------------------------
void __fastcall TfMain::LoadFromBase()
{
int r;
String query_for_all;
query_for_all="SELECT woerter.wort AS wort, lie.lie_wort AS lie," +
			  "truth.truth_wort AS true" +
			  "FROM questions" +
			  "JOIN woerter ON questions.wort_id=woerter.id" +
			  "JOIN truth ON questions.truth_id=truth.id" +
			  "JOIN lie ON questions.lie_id=lie.id" +
			  "WHERE questions.id=";
Randomize;
r=Random(165);
query_for_all=query_for_all + IntToStr(r);
fMain->PgQuery->SQL->Text=query_for_all;
fMain->PgQuery->Open;
wort=fMain->PgQuery->FieldByName("wort")->AsString;
true_wort=fMain->PgQuery->FieldByName("true")->AsString;
false_wort=fMain->PgQuery->FieldByName("lie")->AsString;
umkehr=true;
}
//--------------------------------------------------------------------------
void __fastcall TfMain::Antwort1Click(TObject *Sender)
{
if (umkehr)
{
   fMain->Antwort1->TintColor=TAlphaColor(claRed);
   fMain->Timer1->Enabled=true;
   stopgame=true;
}
else
{
   fMain->Antwort1->TintColor=TAlphaColor(claGreen);
   fMain->Timer1->Enabled=true;
}
}
//---------------------------------------------------------------------------

void __fastcall TfMain::Antwort2Click(TObject *Sender)
{
if (umkehr)
{
   fMain->Antwort2->TintColor=TAlphaColor(claGreen);
   fMain->Timer1->Enabled=true;
}
else
{
   fMain->Antwort2->TintColor=TAlphaColor(claRed);
   fMain->Timer1->Enabled=true;
   stopgame=true;
}
}
//---------------------------------------------------------------------------



void __fastcall TfMain::Timer1Timer(TObject *Sender)
{
   if (stopgame)
   {
	 fMain->Antwort1->Visible=false;
	 fMain->Antwort2->Visible=false;
	 fMain->WortLabel->Visible=false;
	 fMain->Antwort1->TintColor=TAlphaColor(claNull);
	 fMain->Antwort2->TintColor=TAlphaColor(claNull);
	 fMain->MainMenuLabel->Visible=true;
	 fMain->NewGameButton->Visible=true;
	 fMain->AboutButton->Visible=true;
	 fMain->Timer1->Enabled=false;
   }
   else
   {
	 fMain->Antwort1->Visible=false;
	 fMain->Antwort2->Visible=false;
	 fMain->WortLabel->Visible=false;
	 fMain->Antwort1->TintColor=TAlphaColor(claNull);
	 fMain->Antwort2->TintColor=TAlphaColor(claNull);
	 fMain->aind->Visible=true;
	 fMain->LoadingLabel->Visible=true;
	 fMain->LoadFromBase();
	 fMain->aind->Visible=false;
	 fMain->LoadingLabel->Visible=false;
	 fMain->WortLabel->Text=wort;
		if (umkehr)
		{
			fMain->Antwort1->Text=false_wort;
			fMain->Antwort2->Text=true_wort;
		}
		else
		{
			fMain->Antwort1->Text=true_wort;
			fMain->Antwort2->Text=false_wort;
		}
	 fMain->WortLabel->Visible=true;
	 fMain->Antwort1->Visible=true;
	 fMain->Antwort2->Visible=true;
	 fMain->Timer1->Enabled=false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TfMain::FormShow(TObject *Sender)
{
//size of all buttons
fMain->NewGameButton->Width=fMain->Width-16;
fMain->AboutButton->Width=fMain->Width-16;
fMain->Antwort1->Width=fMain->Width-16;
fMain->Antwort2->Width=fMain->Width-16;
fMain->MainMenuLabel->Width=fMain->Width;
fMain->WortLabel->Width=fMain->Width;
//position of all butons
fMain->NewGameButton->Position->Y=(fMain->Height/2)-10;
fMain->AboutButton->Position->Y=(fMain->Height/2)+10;
fMain->Antwort1->Position->Y=(fMain->Height/2);
fMain->Antwort2->Position->Y=(fMain->Height/2)+20;

}
//---------------------------------------------------------------------------

