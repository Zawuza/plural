unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, customdrawncontrols, customdrawn_wince, pqconnection,
  sqldb;

type

  { TForm1 }

  TForm1 = class(TForm)
    NewGameButton: TCDButton;
    AboutButton: TCDButton;
    Antwort1: TCDButton;
    Antwort2: TCDButton;
    PQConnection1: TPQConnection;
    AboutText: TStaticText;
    Timer2: TTimer;
    WordSearch: TSQLQuery;
    MaxId: TSQLQuery;
    SQLTransaction: TSQLTransaction;
    MaxIdTrans: TSQLTransaction;
    Timer1: TTimer;
    WortLabel: TLabel;
    LoadLabel: TLabel;
    Label1: TLabel;
    Shape1: TShape;
    MainMenuLabel: TStaticText;
    procedure AboutButtonClick(Sender: TObject);
    procedure Antwort1Click(Sender: TObject);
    procedure Antwort2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NewGameButtonClick(Sender: TObject);
    procedure LoadFromBase(var Wort,TrueWort,FalseWort:string; var umkehr: boolean);
    procedure GetWordsToControls;
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure ShowNoInternet;
  private
    { private declarations }
  public
    { public declarations }
    Wort,TrueWort,FalseWort: string;
    umkehr,stopgame:boolean;
    max:integer;
  end;

var
  Form1: TForm1;

procedure ObUmlaut(var Str:string);

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.GetWordsToControls;
begin
  if umkehr then
     begin
     Antwort1.Caption:=FalseWort;
     Antwort2.Caption:=TrueWort;
     end
  else
     begin
     Antwort1.Caption:=TrueWort;
     Antwort2.Caption:=FalseWort;
     end;
   WortLabel.Caption:=Wort;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Antwort1.Visible:=false;
  Antwort2.Visible:=false;
  WortLabel.Visible:=false;
  Antwort1.Color:=$00E8E8E8;
  Antwort2.Color:=$00E8E8E8;
  Antwort1.Enabled:=true;
  Antwort2.Enabled:=true;
  Timer1.Enabled:=false;
  if stopgame then
     begin
     MainMenuLabel.Visible:=true;
     NewGameButton.Visible:=true;
     AboutButton.Visible:=true;
     end
  else
     begin
     LoadLabel.Visible:=true;
     LoadFromBase(Wort,TrueWort,FalseWort,umkehr);
     GetWordsToControls;
     LoadLabel.Visible:=false;
     WortLabel.Visible:=true;
     Antwort1.Visible:=true;
     Antwort2.Visible:=true;
     end;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
MainMenuLabel.Visible:=true;
NewGameButton.Visible:=true;
AboutButton.Visible:=true;
AboutText.Caption:='Loading....';
AboutText.Visible:=false;
Timer2.Enabled:=false;;
end;

procedure TForm1.LoadFromBase(var Wort,TrueWort,FalseWort:string; var umkehr: boolean);
var Query:string;
  num:integer;
begin
try
   {In dbase gibt es kein ID=0}
   num:=Random(max);
   Inc(num);
   Query:='SELECT woerter.wort AS wort, lie.lie_wort AS lie, ' +
  			  'truth.truth_wort AS true ' +
  			  'FROM questions ' +
  			  'JOIN woerter ON questions.wort_id=woerter.id ' +
  			  'JOIN truth ON questions.truth_id=truth.id ' +
  			  'JOIN lie ON questions.lie_id=lie.id ' +
  			  'WHERE questions.id='+ IntToStr(num);
   WordSearch.SQL.Text:=Query;
   WordSearch.Open;
   Wort:=WordSearch.FieldByName('wort').AsString;
   TrueWort:=WordSearch.FieldByName('true').AsString;
   FalseWort:=WordSearch.FieldByName('lie').AsString;
   WordSearch.Close;
except
   ShowNoInternet;
   WordSearch.Close;
end;
case Random(2)of
     1: umkehr:=true;
     0: umkehr:=false;
end;
ObUmlaut(Wort);
ObUmlaut(TrueWort);
ObUmlaut(FalseWort);
end;

procedure TForm1.NewGameButtonClick(Sender: TObject);
begin
  NewGameButton.Visible:=false;
  MainMenuLabel.Visible:=false;
  AboutButton.Visible:=false;
  LoadLabel.Visible:=true;
  LoadFromBase(Wort,TrueWort,FalseWort,umkehr);
  LoadLabel.Visible:=false;
  GetWordsToControls;
  WortLabel.Visible:=true;
  Antwort1.Visible:=true;
  Antwort2.Visible:=true;
  stopgame:=false;
end;

procedure TForm1.Antwort1Click(Sender: TObject);
begin
  if umkehr then
     begin
     Antwort1.Color:=$00A4A4FF;
     Antwort2.Enabled:=false;
     Timer1.Enabled:=true;
     stopgame:=true;
     end
  else
     begin
     Antwort1.Color:=$009DFF9D;
     Antwort2.Enabled:=false;
     Timer1.Enabled:=true;
     end;
end;

procedure TForm1.AboutButtonClick(Sender: TObject);
begin
  MainMenuLabel.Visible:=false;
  NewGameButton.Visible:=false;
  AboutButton.Visible:=false;
  AboutText.Caption:='Das Spiel wurde von Andrei Aleksandrov im Jahr 2016 entwickelt';
  AboutText.Visible:=true;
  Timer2.Enabled:=true;
end;

procedure TForm1.Antwort2Click(Sender: TObject);
begin
  if not umkehr then
     begin
     Antwort2.Color:=$00A4A4FF;
     Antwort1.Enabled:=false;
     Timer1.Enabled:=true;
     stopgame:=true;
     end
  else
     begin
     Antwort2.Color:=$009DFF9D;
     Antwort1.Enabled:=false;
     Timer1.Enabled:=true;
     end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
try
   Maxid.Open;
   max:=Maxid.FieldByName('max').AsInteger;
   Maxid.Close;
except
   ShowNoInternet;
   Maxid.Close;
end;
Randomize;
end;

procedure ObUmlaut(var Str:string);
begin
      Str:=StringReplace(str,'s()','ß',[rfReplaceAll]);
      Str:=StringReplace(str,'a()','ä',[rfReplaceAll]);
      Str:=StringReplace(str,'o()','ö',[rfReplaceAll]);
      Str:=StringReplace(str,'u()','ü',[rfReplaceAll]);
      Str:=StringReplace(str,'A()','Ä',[rfReplaceAll]);
      Str:=StringReplace(str,'O()','Ö',[rfReplaceAll]);
      Str:=StringReplace(str,'U()','Ü',[rfReplaceAll]);
end;

procedure TForm1.ShowNoInternet;
var i: integer;
begin
for i:=0 to ComponentCount-1 do
    if not((Components[i]=Shape1) and (Components[i]=Label1)) then
       if Components[i] is TControl then
          (Components[i] as TControl).Visible:=false;
Shape1.Visible:=true;
Label1.Visible:=true;
AboutText.Caption:='NO INTERNET >_<' + #13 + 'Restart the program';
AboutText.Visible:=true;
end;

end.

